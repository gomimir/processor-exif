Binaries in this directory are extracted EXIF data from several cameras.

```go
jmp := jpegstructure.NewJpegMediaParser()
ec, err := jmp.ParseBytes(imageBytes)
if err != nil {
  panic(err)
}

_, exifBytes, err := ec.Exif()
if err != nil {
  panic(err)
}

ioutil.WriteFile("exif.bin", exifBytes, 0644)
```

Note a lot of EXIF samples can be found at: https://exiftool.org/sample_images.html