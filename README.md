# Mimir EXIF extractor

EXIF extractor is a Mimir processor that reads photos and extract summary of their EXIF tags if present. 

For more information about how to use it see [documentation](https://gomimir.gitlab.io/processors/exif-extractor).

## Development

Test the processor on your local file:

```bash
go run cmd/processor-exif/processor.go test extractExif testdata/your_img.jpg
```

Test it together with the rest of the platform (expects you have Mimir + Redis running on your localhost):

```bash
make run
```

Make a docker image to test your work in deployed environment:

```bash
make docker
```

