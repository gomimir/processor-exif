module gitlab.com/gomimir/processor-exif

go 1.16

require (
	github.com/dsoprea/go-exif/v2 v2.0.0-20210131231135-d154f10435cc
	github.com/dsoprea/go-heic-exif-extractor v0.0.0-20200717090456-b3d9dcddffd1
	github.com/dsoprea/go-jpeg-image-structure v0.0.0-20210128210355-86b1014917f2
	github.com/dsoprea/go-tiff-image-structure v0.0.0-20200807080429-5631a803a91b
	github.com/rs/zerolog v1.20.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1
	gitlab.com/gomimir/processor v0.29.0
)

// replace gitlab.com/gomimir/processor => ../processor
