TAG_COMMIT := $(shell git rev-list --abbrev-commit --tags --max-count=1)
TAG := $(shell git describe --abbrev=0 --tags ${TAG_COMMIT} 2>/dev/null || true)
COMMIT := $(shell git rev-parse --short HEAD)
DATE := $(shell git log -1 --format=%cd --date=format:"%Y%m%d")
VERSION := $(TAG:v%=%)

ifeq ($(VERSION),)
    VERSION := 0.0.1-next-$(COMMIT)-$(DATE)
else
	ifneq ($(COMMIT), $(TAG_COMMIT))
			VERSION := $(VERSION)-next-$(COMMIT)-$(DATE)
	endif
endif
ifneq ($(shell git status --porcelain),)
    VERSION := $(VERSION)-dirty
endif

PLATFORMS := linux/amd64 linux/arm64 # darwin/amd64
FLAGS := -ldflags "-X main.version=$(VERSION)"
INPUT := cmd/processor-exif/processor.go

temp = $(subst /, ,$@)
os = $(word 1, $(temp))
arch = $(word 2, $(temp))

version:
	@echo Version: $(VERSION)

docker:
	docker build . --tag registry.gitlab.com/gomimir/processor-exif:v$(VERSION) --build-arg VERSION=$(VERSION)

run:
	go run $(FLAGS) $(INPUT) process

release: $(PLATFORMS)

$(PLATFORMS):
	CGO_ENABLED=0 GOOS=$(os) GOARCH=$(arch) go build $(FLAGS) -o 'bin/release/$(@)/mimir-processor-exif' $(INPUT)

.PHONY: version docker run release $(PLATFORMS)