package exif

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"regexp"
	"strconv"
	"time"

	"github.com/dsoprea/go-exif/v2"
	exifcommon "github.com/dsoprea/go-exif/v2/common"
	mimir "gitlab.com/gomimir/processor"

	heicstructure "github.com/dsoprea/go-heic-exif-extractor"
	jpegstructure "github.com/dsoprea/go-jpeg-image-structure"
	tiffstructure "github.com/dsoprea/go-tiff-image-structure"
)

// ExifSummary - extracted summary of EXIF data
type ExifSummary struct {
	AppleContentIdentifier *string `mimir:"apple_contentId"`

	CameraMake  *string `mimir:"camera_make"`
	CameraModel *string `mimir:"camera_model"`

	LensMake    *string         `mimir:"lens_make"`
	LensModel   *string         `mimir:"lens_model"`
	FocalLength *mimir.Fraction `mimir:"lens_focalLength"`

	Orientation *uint16 `mimir:"image_orientation"`
	ImageWidth  *uint32 `mimir:"image_width"`
	ImageHeight *uint32 `mimir:"image_height"`

	TakeAt *time.Time `mimir:"authoredAt"`

	FNumber      *mimir.Fraction `mimir:"exposure_aperture"`
	ExposureTime *mimir.Fraction `mimir:"exposure_time"`
	ISOSpeed     *uint16         `mimir:"exposure_iso"`
	ExposureBias *mimir.Fraction `mimir:"exposure_bias"`

	GPS *mimir.GPS `mimir:"gps"`
}

func (exif *ExifSummary) ToProperties() *mimir.Properties {
	props := mimir.PropertiesFromStruct(exif)

	props.Set("type", "image")

	if exif.TakeAt != nil {
		props.Set("date", *exif.TakeAt)
	}

	return props
}

var jpegRx = regexp.MustCompile(`(?i)\.(jpg|jpeg)$`)
var heicRx = regexp.MustCompile(`(?i)\.heic$`)
var tiffRx = regexp.MustCompile(`(?i)\.(cr2|arw)$`)

// GetExifBytes - extracts exif byte data out of file content (filename serves only as a hint)
func GetExifBytes(filename string, data []byte) ([]byte, error) {
	if jpegRx.MatchString(filename) {
		jmp := jpegstructure.NewJpegMediaParser()
		ec, err := jmp.ParseBytes(data)
		if err != nil {
			return nil, err
		}

		_, exifData, err := ec.Exif()
		if err != nil {
			return nil, err
		}

		return exifData, nil
	} else if heicRx.MatchString(filename) {
		hmp := heicstructure.NewHeicExifMediaParser()
		ec, err := hmp.ParseBytes(data)
		if err != nil {
			return nil, err
		}

		_, exifData, err := ec.Exif()
		if err != nil {
			return nil, err
		}

		return exifData, nil
	} else if tiffRx.MatchString(filename) {
		tmp := tiffstructure.NewTiffMediaParser()
		ec, err := tmp.ParseBytes(data)
		if err != nil {
			return nil, err
		}

		_, exifData, err := ec.Exif()
		if err != nil {
			return nil, err
		}

		return exifData, nil
	}

	return nil, fmt.Errorf("unsupported file format")
}

// ExtractExif - extracts EXIF summary out of raw exif bytes
func ExtractExif(exifBytes []byte) (*ExifSummary, error) {
	exifTags, err := exif.GetFlatExifData(exifBytes)
	if err != nil {
		return nil, err
	}

	result := &ExifSummary{}

	// for _, tag := range exifTags {
	// 	fmt.Println(tag)
	// }

	// Camera
	result.CameraMake = findStringTagWithID(exifTags, "IFD", 0x10f)
	result.CameraModel = findStringTagWithID(exifTags, "IFD", 0x110)

	// Lens
	result.LensMake = findStringTagWithID(exifTags, "IFD/Exif", 0xa433)
	result.LensModel = findStringTagWithID(exifTags, "IFD/Exif", 0xa434)

	// Orientation
	result.Orientation = findShortTagWithID(exifTags, "IFD", 0x112)

	// Image dimensions
	result.ImageWidth = findLongTagWithID(exifTags, "IFD/Exif", 0xa002)
	result.ImageHeight = findLongTagWithID(exifTags, "IFD/Exif", 0xa003)

	// Date
	if dateTimeOriginal := findTagWithID(exifTags, "IFD/Exif", 0x9003); dateTimeOriginal != nil {
		if value, ok := dateTimeOriginal.Value.(string); ok && value != "0000:00:00 00:00:00" {
			if dateTimeOriginalTime, err := exif.ParseExifFullTimestamp(value); err == nil {
				// Timezone
				tz := time.Now().Local().Location()
				if offsetTimeOriginal := findTagWithID(exifTags, "IFD/Exif", 0x9011); offsetTimeOriginal != nil {
					if value, ok := offsetTimeOriginal.Value.(string); ok {
						if tzFromTag, err := getTimezone(value); err == nil {
							tz = tzFromTag
						}
					}
				}

				dateTimeOriginalTime = time.Date(
					dateTimeOriginalTime.Year(),
					dateTimeOriginalTime.Month(),
					dateTimeOriginalTime.Day(),
					dateTimeOriginalTime.Hour(),
					dateTimeOriginalTime.Minute(),
					dateTimeOriginalTime.Second(),
					dateTimeOriginalTime.Nanosecond(),
					tz,
				)

				// Subsecond
				if subSecTimeOriginal := findTagWithID(exifTags, "IFD/Exif", 0x9291); subSecTimeOriginal != nil {
					if valueStr, ok := subSecTimeOriginal.Value.(string); ok {
						if valueFloat, err := strconv.ParseFloat(fmt.Sprintf("0.%v", valueStr), 64); err == nil {
							dateTimeOriginalTime = dateTimeOriginalTime.Add(time.Duration(valueFloat * float64(time.Second)))
						}
					}
				}

				result.TakeAt = &dateTimeOriginalTime
			}
		}
	}

	// Exposure info
	result.FocalLength = findRationalTagWithID(exifTags, "IFD/Exif", 0x920a)
	result.FNumber = findRationalTagWithID(exifTags, "IFD/Exif", 0x829d)
	result.ExposureTime = findRationalTagWithID(exifTags, "IFD/Exif", 0x829a)
	result.ISOSpeed = findShortTagWithID(exifTags, "IFD/Exif", 0x8827)
	result.ExposureBias = findSignedRationalTagWithID(exifTags, "IFD/Exif", 0x9204)

	// GPS
	if gpsLatitude := findGpsDegreesWithIDs(exifTags, 0x01, 0x02); gpsLatitude != nil {
		if gpsLongitude := findGpsDegreesWithIDs(exifTags, 0x03, 0x04); gpsLongitude != nil {
			result.GPS = &mimir.GPS{
				Latitude:  *gpsLatitude,
				Longitude: *gpsLongitude,
			}
		}
	}

	// MakeNote
	// https://github.com/mostlygeek/goexif-apple-makernotes/blob/master/makernotes/apple.go
	// https://metacpan.org/dist/Image-ExifTool/source/lib/Image/ExifTool/Apple.pm
	if tag := findTagWithID(exifTags, "IFD/Exif", 0x927c); tag != nil {
		if bytes.Equal(tag.ValueBytes[:10], []byte("Apple iOS\000")) {

			im := exif.NewIfdMapping()
			err = im.Add(
				[]uint16{},
				exifcommon.IfdStandardIfdIdentity.TagId(), exifcommon.IfdStandardIfdIdentity.Name())

			if err != nil {
				return nil, err
			}

			ti := exif.NewTagIndex()
			enum := exif.NewIfdEnumerate(im, ti, tag.ValueBytes, binary.BigEndian)

			idx, err := enum.Collect(14)
			if err != nil {
				return nil, err
			}

			for _, e := range idx.RootIfd.Entries {
				if e.TagId() == 0x0011 {
					if val, err := e.Value(); err == nil {
						if str, ok := val.(string); ok {
							result.AppleContentIdentifier = &str
						}
					}
				}
			}
		}
	}

	return result, nil
}

func findGpsDegreesWithIDs(tags []exif.ExifTag, refID uint16, valueID uint16) *float64 {
	if gpsLatitudeRef := findStringTagWithID(tags, "IFD/GPSInfo", refID); gpsLatitudeRef != nil {
		if gpsLatitudeTag := findTagWithID(tags, "IFD/GPSInfo", valueID); gpsLatitudeTag != nil {
			if rationals, ok := gpsLatitudeTag.Value.([]exifcommon.Rational); ok {
				if gd, err := exif.NewGpsDegreesFromRationals(*gpsLatitudeRef, rationals); err == nil {
					d := gd.Decimal()
					return &d
				}
			}
		}
	}

	return nil
}

func findStringTagWithID(tags []exif.ExifTag, ifdPath string, id uint16) *string {
	if tag := findTagWithID(tags, ifdPath, id); tag != nil {
		if value, ok := tag.Value.(string); ok && value != "" {
			return &value
		}
	}

	return nil
}

func findShortTagWithID(tags []exif.ExifTag, ifdPath string, id uint16) *uint16 {
	if tag := findTagWithID(tags, ifdPath, id); tag != nil {
		if value, ok := tag.Value.([]uint16); ok && len(value) == 1 {
			return &value[0]
		}
	}

	return nil
}

func findLongTagWithID(tags []exif.ExifTag, ifdPath string, id uint16) *uint32 {
	if tag := findTagWithID(tags, ifdPath, id); tag != nil {
		if value, ok := tag.Value.([]uint32); ok && len(value) == 1 {
			return &value[0]
		}
	}

	return nil
}

func findRationalTagWithID(tags []exif.ExifTag, ifdPath string, id uint16) *mimir.Fraction {
	if tag := findTagWithID(tags, ifdPath, id); tag != nil {
		if value, ok := tag.Value.([]exifcommon.Rational); ok && len(value) == 1 && value[0].Denominator != 0 {
			return &mimir.Fraction{
				Numerator:   int64(value[0].Numerator),
				Denominator: int64(value[0].Denominator),
			}
		}
	}

	return nil
}

func findSignedRationalTagWithID(tags []exif.ExifTag, ifdPath string, id uint16) *mimir.Fraction {
	if tag := findTagWithID(tags, ifdPath, id); tag != nil {
		if value, ok := tag.Value.([]exifcommon.SignedRational); ok && len(value) == 1 && value[0].Denominator != 0 {
			return &mimir.Fraction{
				Numerator:   int64(value[0].Numerator),
				Denominator: int64(value[0].Denominator),
			}
		}
	}

	return nil
}

func findTagWithID(tags []exif.ExifTag, ifdPath string, id uint16) *exif.ExifTag {
	for _, tag := range tags {
		if tag.TagId == id && tag.IfdPath == ifdPath {
			return &tag
		}
	}

	return nil
}

var tzOffsetRx = regexp.MustCompile(`^([+-])([0-9]{2}):([0-9]{2})$`)

func getTimezone(offset string) (*time.Location, error) {
	m := tzOffsetRx.FindStringSubmatch(offset)
	if len(m) > 0 {
		hours, err := strconv.Atoi(m[2])
		if err != nil {
			return nil, err
		}

		minutes, err := strconv.Atoi(m[3])
		if err != nil {
			return nil, err
		}

		offsetSeconds := (hours*60*60 + minutes*60)
		if m[1] == "-" {
			offsetSeconds = offsetSeconds * -1
		}

		return time.FixedZone(offset, offsetSeconds), nil
	}

	return nil, fmt.Errorf("unable to parse TZ offset %v", offset)
}
