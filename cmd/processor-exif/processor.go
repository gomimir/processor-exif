package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"time"

	"gitlab.com/gomimir/processor-exif/internal/exif"
	mimirapp "gitlab.com/gomimir/processor/pkg/app"
	mimirpb "gitlab.com/gomimir/processor/protobuf"
	mimirpbconv "gitlab.com/gomimir/processor/protobufconv"
)

// populated by build flags
var version string

func main() {
	app := mimirapp.NewProcessorApp("mimir-processor-exif", version)

	app.RegisterFileProcessor("extractExif", handler)
	app.SetDefaultProcessingQueue("extractExif")

	app.Execute()
}

func handler(req mimirapp.FileProcessorRequest) error {
	reader, err := req.Task.File().Reader()
	if err != nil {
		req.Log.Error().Err(err).Msg("Unable read file")
		return err
	}

	defer reader.Close()

	bytes, err := ioutil.ReadAll(reader)
	if err != nil {
		req.Log.Error().Err(err).Msg("Error occurred while reading the file")
		return err
	}

	// Get EXIF bytes
	exifBytes, err := exif.GetExifBytes(req.Task.File().FileRef().Filename, bytes)
	if err != nil {
		// Note: err == goexif.ErrNoExif does not work
		if err.Error() == "no exif data" {
			req.Log.Warn().Msg("No EXIF data")
			return nil
		} else {
			req.Log.Error().Err(err).Msg("Unable to extract EXIF bytes")
			return err
		}
	}

	// Extract EXIF information
	extracted, err := exif.ExtractExif(exifBytes)
	if err != nil {
		req.Log.Error().Err(err).Msg("Unable to extract EXIF tags")
		return err
	}

	// Build Mimir properties
	props := extracted.ToProperties()

	if !req.IsTest {
		// Report extracted properties
		start := time.Now()

		convertedProps, err := mimirpbconv.PropertiesToProtoBuf(*props)
		if err != nil {
			req.Log.Error().Err(err).Msg("Failed to process asset properties")
			return err
		}

		ctx, cancel := context.WithTimeout(req.Command.Context(), 5*time.Second)
		defer cancel()

		_, err = req.Client.UpdateAssets(ctx, &mimirpb.UpdateAssetsRequest{
			Selector: &mimirpb.UpdateAssetsRequest_Ref{
				Ref: &mimirpb.AssetRef{
					Index:   req.Task.File().AssetRef().IndexName(),
					AssetId: req.Task.File().AssetRef().AssetID(),
				},
			},
			UpdateFiles: []*mimirpb.UpdateAssetFileRequest{
				{
					Selector: &mimirpb.UpdateAssetFileRequest_Ref{
						Ref: &mimirpb.FileRef{
							Repository: req.Task.File().FileRef().Repository,
							Filename:   req.Task.File().FileRef().Filename,
						},
					},
					SetProperties: convertedProps,
				},
			},
		})

		duration := time.Since(start)

		if err != nil {
			req.Log.Error().Err(err).Msg("Failed to update asset properties")
			return err
		}

		req.Log.Debug().Str("took", fmt.Sprintf("%v", duration)).Msg("Assets updated")
	} else {
		fmt.Println(props.String())
	}

	return nil
}
